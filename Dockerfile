FROM node:14-alpine

WORKDIR /usr/src/app

COPY package*.json ./


#RUN npm cache clean --force
#RUN npm cache verify
RUN apk add git tzdata
ENV TZ="Asia/Ho_Chi_Minh"
RUN npm i -g ts-node
RUN npm install

COPY . .
WORKDIR /usr/src/app/
EXPOSE 3000

CMD [ "npm", "run", "dev" ]

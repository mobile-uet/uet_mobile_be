## How to run in development

```bash
# 0. install xampp, create database
# 1. project installation
$ npm install -g ts-node
$ npm install -g tsc
$ npm install
# 2. run project
$ npm run dev
# 3. import dev_mobile.sql to database dev_mobile
# 4. in vs-code terminal, type rs
$ rs
```

## Installation

```bash
$ npm install -g ts-node
$ npm install
```

## Build project

```bash
$ npm install -g tsc
$ npm run build
```

## Running the app

```bash
# development
$ npm run dev

# production mode
$ npm run start
```

## Database

```bash
dev_mobile
```

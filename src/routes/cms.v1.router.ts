import { Router } from 'express'
import userRouter from './table_router/user.router'
import orderRouter from './table_router/order.router'
import productPhotoRouter from './table_router/productPhoto.router'
import productSizeRouter from './table_router/productSize.router'
import productLineRouter from './table_router/productLine.router'
import orderDetailRouter from './table_router/orderDetail.router'
import productRouter from './table_router/product.router'
import productLineController from '../controllers/productLine.controller'
import productSizeController from '../controllers/productSize.controller'
import orderController from '../controllers/order.controller'
import orderDetailController from '../controllers/orderDetail.controller'
const router = Router()

router.use('/order', orderRouter)
router.use('/orderDetail', orderDetailRouter)
router.use('/product', productRouter)
router.use('/productLine', productLineRouter)
router.use('/productPhoto', productPhotoRouter)
router.use('/productSize', productSizeRouter)
router.use('/user', userRouter)

// order Order api
router.get('/order', orderController.getOrders)
router.get('/order/:id', orderController.getOrder)

// orderDetail OrderDetail api
router.get('/orderDetail', orderDetailController.getOrderDetails)
router.get('/orderDetail/:id', orderDetailController.getOrderDetail)

router.get('/size', productSizeController.getSizes)

export default router

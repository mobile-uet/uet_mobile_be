import { Router } from 'express'
import userController from '../controllers/user.controller'
import orderController from '../controllers/order.controller'
import productSizeController from '../controllers/productSize.controller'
import productLineController from '../controllers/productLine.controller'
import orderDetailController from '../controllers/orderDetail.controller'
import productController from '../controllers/product.controller'
import productPhotoController from '../controllers/productPhoto.controller'
const router = Router()

// product Product api
router.get('/product', productController.getProducts)
router.get('/product/:id', productController.getProduct)

// productLine ProductLine api
router.get('/productLine', productLineController.getProductLines)
router.get('/productLine/:id', productLineController.getProductLine)

// productPhoto ProductPhoto api
router.get('/productPhoto', productPhotoController.getProductPhotos)
router.get('/productPhoto/:id', productPhotoController.getProductPhoto)

// productSize ProductSize api
router.get('/productSize', productSizeController.getProductSizes)
router.get('/productSize/:id', productSizeController.getProductSize)
router.get('/size', productSizeController.getSizes)

export default router

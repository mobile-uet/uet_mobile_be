import { Router } from 'express'
import { Request, Response } from 'express'
import orderController from '../../controllers/order.controller'

const router = Router()

router.get('/', orderController.getOrders)
// router.get('/columns', orderController.getColumns)
router.get('/cart', orderController.getCart)
router.get('/:id', orderController.getOrder)
router.post('/', orderController.postOrder)
router.put('/:id', orderController.putOrder)
// router.delete('/:id', orderController.deleteOrder)

export default router

import { Router } from 'express'
import { Request, Response } from 'express'
import productSizeController from '../../controllers/productSize.controller'

const router = Router()

router.get('/', productSizeController.getProductSizes)
// router.get('/columns', productSizeController.getColumns)
router.get('/:id', productSizeController.getProductSize)
// router.post('/', productSizeController.postProductSize)
// router.put('/:id', productSizeController.updateProductSize)
// router.delete('/:id', productSizeController.deleteProductSize)

export default router

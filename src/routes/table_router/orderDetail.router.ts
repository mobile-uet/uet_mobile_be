import { Router } from 'express'
import { Request, Response } from 'express'
import orderDetailController from '../../controllers/orderDetail.controller'

const router = Router()

router.get('/', orderDetailController.getOrderDetails)
// router.get('/columns', orderDetailController.getColumns)
router.get('/:id', orderDetailController.getOrderDetail)
// router.post('/', orderDetailController.postOrderDetail)
// router.put('/:id', orderDetailController.updateOrderDetail)
// router.delete('/:id', orderDetailController.deleteOrderDetail)

export default router

import { Router } from 'express'
import { Request, Response } from 'express'
import productController from '../../controllers/product.controller'

const router = Router()

router.get('/', productController.getProducts)
// router.get('/columns', productController.getColumns)
router.get('/:product', productController.getProduct)
// router.post('/', productController.postProduct)
// router.put('/:id', productController.updateProduct)
// router.delete('/:id', productController.deleteProduct)

export default router

import { Router } from 'express'
import { Request, Response } from 'express'
import productLineController from '../../controllers/productLine.controller'

const router = Router()

router.get('/', productLineController.getProductLines)
// router.get('/columns', productLineController.getColumns)
router.get('/:id', productLineController.getProductLine)
// router.post('/', productLineController.postProductLine)
// router.put('/:id', productLineController.updateProductLine)
// router.delete('/:id', productLineController.deleteProductLine)

export default router

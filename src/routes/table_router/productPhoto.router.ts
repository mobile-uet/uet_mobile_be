import { Router } from 'express'
import { Request, Response } from 'express'
import productPhotoController from '../../controllers/productPhoto.controller'

const router = Router()

router.get('/', productPhotoController.getProductPhotos)
// router.get('/columns', productPhotoController.getColumns)
router.get('/:id', productPhotoController.getProductPhoto)
// router.post('/', productPhotoController.postProductPhoto)
// router.put('/:id', productPhotoController.updateProductPhoto)
// router.delete('/:id', productPhotoController.deleteProductPhoto)

export default router

import authController from '../controllers/auth.controller'
import sideController from '../controllers/side'
import userController from '../controllers/user.controller'
import auth from '../middlewares/auth'
import apiV1Router from './api.v1.router'
import cmsV1Router from './cms.v1.router'

export default function routes(app) {
  app.get('/', sideController.getSwaggerFile)
  app.post('/register', userController.postUser)
  app.post('/login', authController.login)
  app.use('/api/v1', apiV1Router)

  // need login
  app.use('/', auth.checkToken)
  app.post('/logout', authController.logout)
  app.use('/cms/v1', cmsV1Router)

  app.use('/', sideController.catch404)
}

import { Request, Response } from 'express'
import { Order } from '../database/entity/Order.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { User } from '../database/entity/User.entity'
import { OrderDetail } from '../database/entity/OrderDetail.entity'
import { Product } from '../database/entity/Product.entity'

export default {
  // [GET] /order/?page=1
  getOrders: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const orderRepository = AppDataSource.getRepository(Order)
    const result = await helper.paginationDoubleJoin(
      orderRepository,
      'customer',
      pageNumber,
      pageSize,
      `customer.email = '${req.session.email}'`
    )

    // filter user
    result.data.forEach((item) => {
      item.customer = helper.filterPropUser(item.customer)
    })
    res.json(result)
  },

  // [GET] /order/:id
  getOrder: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const orderRepository = AppDataSource.getRepository(Order)
    const result = await orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect(`order.orderDetails`, 'orderDetails')
      .leftJoinAndSelect(`orderDetails.product`, 'product')
      .where('order.id = :id')
      .setParameter('id', id)
      .getOne()
    res.json(result)
  },

  // [GET] /order/cart
  getCart: async function (req: Request, res: Response) {
    const orderRepository = AppDataSource.getRepository(Order)
    const result = await orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect(`order.orderDetails`, 'orderDetails')
      .leftJoinAndSelect(`orderDetails.product`, 'product')
      .where('order.status = "Cart"')
      .andWhere('order.customerEmail = :email')
      .setParameter('email', req.session.email)
      .getOne()
    if (result) res.json(result)
    else {
      const customerRepository = AppDataSource.getRepository(User)
      try {
        const order = new Order()

        order.order_date = new Date()
        order.status = 'Cart'
        order.customer = await customerRepository.findOneBy({
          email: req.session.email,
        })
        await orderRepository.save(order)

        const result = await orderRepository
          .createQueryBuilder('order')
          .leftJoinAndSelect(`order.orderDetails`, 'orderDetails')
          .leftJoinAndSelect(`orderDetails.product`, 'product')
          .where('order.status = "Cart"')
          .andWhere('order.customerEmail = :email')
          .setParameter('email', req.session.email)
          .getOne()
        res.json(result)
      } catch (error) {}
    }
  },

  // [POST] /order
  postOrder: async function (req: Request, res: Response) {
    const orderRepository = AppDataSource.getRepository(Order)
    const customerRepository = AppDataSource.getRepository(User)
    try {
      const order = new Order()

      order.order_date = new Date()
      order.status = 'Cart'
      order.customer = await customerRepository.findOneBy({
        email: req.session.email,
      })

      // validate
      await helper.validate(res, order)
      const result = await orderRepository.save(order)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /order/:id
  putOrder: async function (req: Request, res: Response) {
    const orderRepository = AppDataSource.getRepository(Order)
    const orderDetailsRepository = AppDataSource.getRepository(OrderDetail)
    const productRepository = AppDataSource.getRepository(Product)
    const id = Number(req.params.id)

    try {
      // set order status
      const order = await orderRepository.findOneBy({ id: id })
      order.order_date = new Date()
      order.status = req.body.status
      // validate
      await helper.validate(res, order)
      const result = await orderRepository.save(order)

      // delete all orderdetails
      const deleteResult = await AppDataSource.createQueryBuilder()
        .delete()
        .from(OrderDetail)
        .where(`orderId = :id`, { id: id })
        .execute()
      console.log(deleteResult)

      // set orderdetails
      const orders = req.body.orders
      const orderResult = []
      orders.forEach(async (order) => {
        // create new orderDetail
        const orderDetail = new OrderDetail()
        orderDetail.product = await productRepository.findOneBy({
          id: order.productId,
        })
        orderDetail.order = await orderRepository.findOneBy({
          id: id,
        })
        orderDetail.quantityOrdered = order.quantityOrdered
        orderResult.push(await orderDetailsRepository.save(orderDetail))
      })

      // get orderdetail
      const orderDetails = await orderRepository
        .createQueryBuilder('order')
        .leftJoinAndSelect(`order.orderDetails`, 'orderDetails')
        .where('order.id = :id')
        .setParameter('id', id)
        .getOne()

      res.json({
        msg: 'Update complete',
        result: JSON.stringify(result),
        orderDetails: JSON.stringify(orderDetails),
      })
    } catch (error) {}
  },
}

import { Request, Response } from 'express'
import { User } from '../database/entity/User.entity'
import AppDataSource from '../database/connection'
import helper from './helper'

export default {
  // [GET] /user?page=1
  getUsers: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const userRepository = AppDataSource.getRepository(User)
    const result = await helper.pagination(userRepository, pageNumber, pageSize)

    // // filter user
    // const data = []
    // result.data.forEach((user) => {
    //   data.push(helper.filterPropUser(user))
    // })
    // result.data = data
    res.json(result)
  },

  // [GET] /user/:email
  getUser: async function (req: Request, res: Response) {
    const email = req.params.email
    const user = await AppDataSource.getRepository(User).findOneBy({
      email: email,
    })
    res.json(user)
  },

  // [GET] /user/info
  getSelfInfo: async function (req: Request, res: Response) {
    const email = req.session.email
    const user = await AppDataSource.getRepository(User).findOneBy({
      email: email,
    })
    res.send(helper.filterPropUser(user))
  },

  // [POST] /user
  postUser: async function (req: Request, res: Response) {
    const userRepository = AppDataSource.getRepository(User)

    try {
      const user = new User()

      user.username = String(req.body.username || 'No name')
      user.email = String(req.body.email)
      user.role = String(req.body.role || 'customer')
      user.password = String(req.body.password)

      // validate
      await helper.validate(res, user)
      const result = await userRepository.save(user)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /user/email
  putUser: async function (req: Request, res: Response) {
    const userRepository = AppDataSource.getRepository(User)
    const email = req.params.email
    try {
      const user = await userRepository.findOneBy({ email: email })

      user.username = String(
        req.body.username ? req.body.username : user.username
      )
      user.role = String(req.body.role ? req.body.role : req.session.role)
      user.password = String(
        req.body.password ? req.body.password : user.password
      )

      // validate
      await helper.validate(res, user)
      const result = await userRepository.save(user)
      res.json({
        msg: 'Update complete',
        result: helper.filterPropUser(result),
      })
    } catch (error) {}
  },
}

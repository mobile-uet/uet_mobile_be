import { validate } from 'class-validator'
import { Request, Response } from 'express'
import { User } from '../database/entity/User.entity'
import AppDataSource from '../database/connection'
import { OrderDetail } from '../database/entity/OrderDetail.entity'

const helper = {
  async getAmount(tableRepository, getAmount = 'getOne', where = '1') {
    const data = await tableRepository
      .createQueryBuilder('alias')
      .where(where)
      [getAmount]()
    return data
  },

  async getAmountDoubleJoin(
    tableRepository,
    constraint,
    getAmount = 'getOne',
    where = '1'
  ) {
    const data = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect(`alias.${constraint}`, String(constraint))
      .where(where)
      [getAmount]()
    return data
  },

  async getAmountTripleJoin(
    tableRepository,
    constraints,
    getAmount = 'getOne',
    where = '1'
  ) {
    const data = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect(`alias.${constraints[0]}`, String(constraints[0]))
      .leftJoinAndSelect(`alias.${constraints[1]}`, String(constraints[1]))
      .where(where)
      [getAmount]()
    return data
  },

  async getAmountQuatarJoin(
    tableRepository,
    constraints,
    getAmount = 'getOne',
    where = '1'
  ) {
    const data = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect(`alias.${constraints[0]}`, String(constraints[0]))
      .leftJoinAndSelect(`alias.${constraints[1]}`, String(constraints[1]))
      .leftJoinAndSelect(`alias.${constraints[2]}`, String(constraints[2]))
      .where(where)
      [getAmount]()
    return data
  },

  async pagination(tableRepository, pageNumber, pageSize = 50, where = '1') {
    if (pageSize > 50) pageSize = 50
    const [data, count] = await tableRepository
      .createQueryBuilder('alias')
      .where(where)
      .skip(Number((pageNumber - 1) * pageSize))
      .take(pageSize)
      .getManyAndCount()
    return Object.assign({}, { total: count, data: data })
  },

  async paginationDoubleJoin(
    tableRepository,
    constraint,
    pageNumber,
    pageSize = 50,
    where = '1'
  ) {
    if (pageSize > 50) pageSize = 50
    const [data, count] = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect(`alias.${constraint}`, String(constraint))
      .where(where)
      .skip(Number((pageNumber - 1) * pageSize))
      .take(pageSize)
      .getManyAndCount()
    return Object.assign({}, { total: count, data: data })
  },

  async paginationTripleJoin(
    tableRepository,
    constraints,
    pageNumber,
    pageSize = 50,
    where = '1'
  ) {
    if (pageSize > 50) pageSize = 50
    const [data, count] = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect(`alias.${constraints[0]}`, String(constraints[0]))
      .leftJoinAndSelect(`alias.${constraints[1]}`, String(constraints[1]))
      .where(where)
      .skip(Number((pageNumber - 1) * pageSize))
      .take(pageSize)
      .getManyAndCount()
    return Object.assign({}, { total: count, data: data })
  },

  async paginationQuatarJoin(
    tableRepository,
    constraints,
    pageNumber,
    pageSize = 50,
    where = '1'
  ) {
    if (pageSize > 50) pageSize = 50
    const [data, count] = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect(`alias.${constraints[0]}`, String(constraints[0]))
      .leftJoinAndSelect(`alias.${constraints[1]}`, String(constraints[1]))
      .leftJoinAndSelect(`alias.${constraints[2]}`, String(constraints[2]))
      .where(where)
      .skip(Number((pageNumber - 1) * pageSize))
      .take(pageSize)
      .getManyAndCount()
    return Object.assign({}, { total: count, data: data })
  },

  filterPropUser(user) {
    const result = {
      username: user.username,
      email: user.email,
      googleId: user.googleId,
    }
    return result
  },

  filterConstraints(error) {
    const constraints = []
    error.forEach((err) => {
      for (let key in err.constraints) {
        constraints.push(err.constraints[key])
      }
    })
    return constraints
  },

  async checkFindOne(res, repository, where, errorMsg: any = 'Invalid id') {
    try {
      const result = await repository.findOneBy(where)
      if (!result) throw new Error()
      return result
    } catch (error) {
      res.json({
        msg: 'Invalid id',
        error: errorMsg,
      })
      throw new Error('Invalid id')
    }
  },

  async validate(res, item) {
    const errors = await validate(item)
    if (errors.length > 0) {
      res.json({
        msg: `Validation failed!`,
        error: helper.filterConstraints(errors),
      })
      throw new Error(`Validation failed!`)
    }
  },

  async getOrderDetailById(id) {
    const orderDetailRepository = await AppDataSource.getRepository(OrderDetail)
    const result = await helper.getAmountTripleJoin(orderDetailRepository, [
      'order',
      'product',
    ])
    return result
  },
}

export default helper

import { Request, Response } from 'express'
import { ProductSize } from '../database/entity/ProductSize.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { Product } from '../database/entity/Product.entity'

export default {
  // [GET] /size
  getSizes: async function (req: Request, res: Response) {
    const result = [
      { name: 'S' },
      { name: 'M' },
      { name: 'L' },
      { name: 'XL' },
      { name: 'XXL' },
    ]
    res.json(result)
  },

  // [GET] /productSize/?page=1
  getProductSizes: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const productSizeRepository = AppDataSource.getRepository(ProductSize)
    const result = await helper.pagination(
      productSizeRepository,
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /productSize/:id
  getProductSize: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const productSize = await AppDataSource.getRepository(
      ProductSize
    ).findOneBy({
      id: id,
    })
    res.json(productSize)
  },
}

import { Request, Response } from 'express'
import { OrderDetail } from '../database/entity/OrderDetail.entity'
import AppDataSource from '../database/connection'
import helper from './helper'

export default {
  // [GET] /orderDetail/?page=1
  getOrderDetails: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const orderDetailRepository = AppDataSource.getRepository(OrderDetail)
    const result = await helper.paginationTripleJoin(
      orderDetailRepository,
      ['order', 'product'],
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /orderDetail/:id
  getOrderDetail: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const orderDetailRepository = AppDataSource.getRepository(OrderDetail)
    const result = await helper.getAmountTripleJoin(orderDetailRepository, [
      'order',
      'product',
    ])
    res.json(result)
  },
}

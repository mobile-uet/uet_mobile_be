import { Request, Response } from 'express'
import { ProductPhoto } from '../database/entity/ProductPhoto.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { Product } from '../database/entity/Product.entity'

export default {
  // [GET] /productPhoto/?page=1
  getProductPhotos: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const productPhotoRepository = AppDataSource.getRepository(ProductPhoto)
    const result = await helper.pagination(
      productPhotoRepository,
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /productPhoto/:id
  getProductPhoto: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const productPhoto = await AppDataSource.getRepository(
      ProductPhoto
    ).findOneBy({
      id: id,
    })
    res.json(productPhoto)
  },
}

import { Router } from 'express'
import { Request, Response } from 'express'
import { User } from '../database/entity/User.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { helper as authHelper } from '../middlewares/auth'

export default {
  login: async (req: Request, res: Response) => {
    const email = req.body.email
    const password = req.body.password
    if (!email || !password) {
      res.json({ msg: 'Email or password incorrect' })
      return
    }
    const userRepository = AppDataSource.getRepository(User)
    const user = await userRepository.findOneBy({ email, password })
    let result = { msg: 'Email or password incorrect' }
    if (user) {
      req.session.email = user.email
      req.session.username = user.username
      req.session.role = user.role
      const filtered = helper.filterPropUser(user)
      authHelper.setToken(req, filtered)
      result.msg = 'Login success'
      result = Object.assign({}, result, { data: filtered })
    }
    res.json(result)
  },
  logout: (req: Request, res: Response) => {
    req.session.destroy((err) => {
      if (err) console.log('Logout-error: ', err)
    })
    res.json({ msg: 'Lougout success' })
  },
}

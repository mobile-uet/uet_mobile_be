import { Request, Response } from 'express'
import { Product } from '../database/entity/Product.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { ProductSize } from '../database/entity/ProductSize.entity'

export default {
  // [GET] /product?page=1
  getProducts: async function (req: Request, res: Response) {
    const productLine = Number(req.query.productLine) || 0
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const priceFrom = Number(req.query.priceFrom) || 0
    const priceTo = Number(req.query.priceTo) || 10000000
    // const size = String(req.query.size ? req.query.size : '')
    // if (size) where += ` and productSizes.size = '${size}'`
    const sizes = String(req.query.size ? req.query.size : '').split(',')
    let where = `alias.price >= ${priceFrom} and alias.price <= ${priceTo}`
    let whereSize = ''
    if (sizes[0]) {
      sizes.forEach((size, index) => {
        if (index == 0) {
          whereSize += ` and ( productSizes.size = '${size}'`
        } else {
          whereSize += ` or productSizes.size = '${size}'`
        }
      })
      whereSize += ')'
    }
    where += whereSize
    if (productLine) where += ` and productLine.id = ${productLine}`

    console.log(where)
    const productRepository = AppDataSource.getRepository(Product)
    const result = await helper.paginationQuatarJoin(
      productRepository,
      ['productLine', 'productSizes', 'productPhotos'],
      pageNumber,
      pageSize,
      where
    )
    res.json(result)
  },

  // [GET] /product/:id
  getProduct: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const productRepository = await AppDataSource.getRepository(Product)
    const result = await helper.getAmountQuatarJoin(
      productRepository,
      ['productLine', 'productSizes', 'productPhotos'],
      'getOne',
      `alias.id = ${id}`
    )
    res.json(result)
  },
}

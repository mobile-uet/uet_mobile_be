import { Request, Response } from 'express'
import { ProductLine } from '../database/entity/ProductLine.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { Product } from '../database/entity/Product.entity'

export default {
  // [GET] /productLine/?page=1
  getProductLines: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 50
    const productLineRepository = AppDataSource.getRepository(ProductLine)
    const result = await helper.pagination(
      productLineRepository,
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /productLine/:id
  getProductLine: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const productLine = await AppDataSource.getRepository(
      ProductLine
    ).findOneBy({
      id: id,
    })
    res.json(productLine)
  },
}

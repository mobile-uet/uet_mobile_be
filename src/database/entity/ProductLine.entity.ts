import { IsInt, IsString, Min, IsBoolean } from 'class-validator'
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Product } from './Product.entity'

@Entity()
export class ProductLine {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id: number

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  name: string

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  imageUrl: string

  @OneToMany((type) => Product, (product) => product.productLine)
  products: Product[]
}

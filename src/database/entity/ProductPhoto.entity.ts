import { IsInt, IsString, Min, IsBoolean, IsUrl } from 'class-validator'
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Product } from './Product.entity'

@Entity()
export class ProductPhoto {
  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne((type) => Product, (product) => product.productPhotos)
  product: number

  @Column({ type: 'text' })
  @IsUrl()
  url: string
}

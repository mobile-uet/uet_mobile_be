import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  OneToOne,
} from 'typeorm'
import { IsNumber } from 'class-validator'
import { Order } from './Order.entity'
import { Product } from './Product.entity'

@Entity()
export class OrderDetail {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id: number

  @Column({ type: 'int', unsigned: true, default: 0 })
  @IsNumber()
  quantityOrdered: number

  @ManyToOne((type) => Product, (product) => product.id)
  product: Product

  @ManyToOne((type) => Order, (order) => order.id)
  order: Order
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  OneToMany,
  IsNull,
} from 'typeorm'

import {
  IsBoolean,
  IsDate,
  IsInt,
  IsNumber,
  IsString,
  IsUrl,
  Min,
} from 'class-validator'
import { ProductLine } from './ProductLine.entity'
import { ProductSize } from './ProductSize.entity'
import { ProductPhoto } from './ProductPhoto.entity'

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'boolean', default: false })
  @IsBoolean()
  active: boolean

  @ManyToOne((type) => ProductLine, (productLine) => productLine.products)
  productLine: ProductLine

  @OneToMany((type) => ProductSize, (productSize) => productSize.product)
  productSizes: ProductSize[]

  @OneToMany((type) => ProductPhoto, (productPhoto) => productPhoto.product)
  productPhotos: ProductPhoto[]

  @Column({ type: 'varchar', length: 255, nullable: true })
  @IsString()
  name: string

  @Column({ type: 'text' })
  @IsString()
  description: string

  @Column({ type: 'int', unsigned: true, default: 0 })
  @IsNumber()
  price: number

  @Column({ type: 'int', unsigned: true, default: 0 })
  @IsNumber()
  quantityInStock: number

  // @BeforeInsert()
  // updatePublishedBeforeInsert() {
  //   this.publishedAt = new Date()
  // }

  // @AfterUpdate()
  // updatePublishedBeforeUpdate() {
  //   if (!this.publishedAt) this.publishedAt = new Date()
  // }
}

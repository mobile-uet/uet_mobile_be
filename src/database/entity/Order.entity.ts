import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  IsNull,
  OneToMany,
} from 'typeorm'

import { User } from './User.entity'
import { OrderDetail } from './OrderDetail.entity'

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne((type) => User, (user) => user.email)
  customer: User

  @Column({ type: 'datetime', nullable: true })
  order_date: Date

  @Column({ type: 'varchar', length: 255, default: 'Cart', nullable: true })
  status: String

  @OneToMany((type) => OrderDetail, (orderDetail) => orderDetail.order)
  orderDetails: OrderDetail[]

  @BeforeInsert()
  updatePublishedBeforeInsert() {
    this.order_date = new Date()
  }
}

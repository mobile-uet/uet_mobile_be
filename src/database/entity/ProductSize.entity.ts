import { IsInt, IsString, Min, IsBoolean } from 'class-validator'
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Product } from './Product.entity'

@Entity()
export class ProductSize {
  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne((type) => Product, (product) => product.productSizes)
  product: number

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  size: string
}
